# Bubbleteam Resources

Dieses Repository dient als Sammelbecken für alle Daten und Dateien rund um das Bubbleteam.

Das Team koordiniert sich im Matrix-Raum [#Bubbleteam:matrix.org](https://matrix.to/#/#Bubbleteam:matrix.org). Rückfragen gerne dort.

Das [verwendete Photo](https://pixabay.com/photos/soap-bubble-floats-coloured-361574/) stammt von Pixabay und ist Public Domain.

